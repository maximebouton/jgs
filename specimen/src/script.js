// FAMILY
const jgs5 = document.getElementById("jgs5");
jgs5.addEventListener('input', function(e){
  if (this.checked) {
    document.body.classList.add('jgs5');
    document.body.classList.remove('jgs9');
  }
});
const jgs9 = document.getElementById("jgs9");
jgs9.addEventListener('input', function(e){
  if (this.checked) {
    document.body.classList.remove('jgs5');
    document.body.classList.add('jgs9');
  }
});

// SIZE
const x1 = document.getElementById("x1");
x1.addEventListener('input', function(e){
  if (this.checked) {
    document.body.classList.add('x1');
    document.body.classList.remove('x2');
    document.body.classList.remove('x3');
  }
});
const x2 = document.getElementById("x2");
x2.addEventListener('input', function(e){
  if (this.checked) {
    document.body.classList.remove('x1');
    document.body.classList.add('x2');
    document.body.classList.remove('x3');
  }
});
const x3 = document.getElementById("x3");
x3.addEventListener('input', function(e){
  if (this.checked) {
    document.body.classList.remove('x1');
    document.body.classList.remove('x2');
    document.body.classList.add('x3');
  }
});

// MODE
const day = document.getElementById("day");
day.addEventListener('input', function(e){
  if (this.checked) {
    document.body.classList.add('day');
    document.body.classList.remove('night');
  }
});
const night = document.getElementById("night");
night.addEventListener('input', function(e){
  if (this.checked) {
    document.body.classList.remove('day');
    document.body.classList.add('night');
  }
});

[jgs5,jgs9,x1,x2,x3,day,night].forEach(input => {
  if (input.checked) {
    var event = document.createEvent('Event');
    event.initEvent('input', true, true);
    input.dispatchEvent(event);
  }
});
